<?php

declare(strict_types=1);

namespace Domain\Payment\Service;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;
use Domain\Payment\Contract\PaymentTypeSelectorInterface;
use Domain\Payment\Factory\ConditionFactoryInterface;
use Domain\Payment\Repository\PaymentSystemRepositoryInterface;

final class PaymentTypeSelector implements PaymentTypeSelectorInterface
{
    public function __construct(
        private readonly PaymentSystemRepositoryInterface $repository,
        private readonly ConditionFactoryInterface $factory
    ) {
    }

    public function getButtons(
        string $productType,
        string $amount,
        string $lang,
        string $countryCode,
        string $userOs
    ): array {
        $paymentSystems = $this->repository->findAll();
        $result = [];

        /**
         * @var PaymentSystem $paymentSystem
         * @var PaymentMethodInterface $paymentMethod
         */

        foreach ($paymentSystems as $paymentSystem) {
            foreach ($paymentSystem->getPaymentMethods() as $paymentMethod) {
                $condition = $this->factory->create(
                    $productType,
                    $amount,
                    $lang,
                    $countryCode,
                    $userOs,
                    $paymentSystem,
                    $paymentMethod
                );

                if ($condition->isSatisfyBy($paymentSystem, $paymentMethod)) {
                    $result[] = $paymentMethod;
                }
            }
        }

        // here we can make sort by countries priority
        // PaymentMethodSorter->sort($result);

        return $result;
    }
}