<?php

declare(strict_types=1);

namespace Domain\Payment\Contract;

interface PaymentTypeSelectorInterface
{
    public function getButtons(
        string $productType,
        string $amount,
        string $lang,
        string $countryCode,
        string $userOs
    ): array; // better use collection to guarantee data structure for return type
}