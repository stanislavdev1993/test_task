<?php

declare(strict_types=1);

namespace Domain\Payment\Factory;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;
use Domain\Payment\Condition\PaymentConditionInterface;

interface ConditionFactoryInterface
{
    public function create(
        string $productType,
        string $amount,
        string $lang,
        string $countryCode,
        string $userOs,
        PaymentSystem $paymentSystem,
        PaymentMethodInterface $paymentMethod
    ): PaymentConditionInterface;
}