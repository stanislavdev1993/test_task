<?php

declare(strict_types=1);

namespace Domain\Payment\Factory;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;
use Domain\Payment\Condition\CompositeCondition;
use Domain\Payment\Condition\PaymentConditionInterface;
use Domain\Payment\Condition\PaymentMethod\PaymentMethodEnabledCondition;

final class DefaultConditionFactory implements ConditionFactoryInterface
{
    public function create(
        string $productType,
        string $amount,
        string $lang,
        string $countryCode,
        string $userOs,
        PaymentSystem $paymentSystem,
        PaymentMethodInterface $paymentMethod
    ): PaymentConditionInterface {
        return new CompositeCondition(
            new PaymentMethodEnabledCondition(true),
            // here other condition combinations
        );
    }
}