<?php

declare(strict_types=1);

namespace Domain\Payment\Condition;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;

interface PaymentConditionInterface
{
    public function isSatisfyBy(PaymentSystem $paymentSystem, PaymentMethodInterface $paymentMethod): bool;
}