<?php

declare(strict_types=1);

namespace Domain\Payment\Condition\PaymentMethod;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;
use Domain\Payment\Condition\PaymentConditionInterface;

final class PaymentSystemEnabledCondition implements PaymentConditionInterface
{
    public function __construct(private readonly bool $enabled)
    {
    }

    public function isSatisfyBy(PaymentSystem $paymentSystem, PaymentMethodInterface $paymentMethod): bool
    {
        return $paymentSystem->getSettings()->isEnabled() === $this->enabled;
    }
}