<?php

declare(strict_types=1);

namespace Domain\Payment\Condition;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;

final class AndCondition implements PaymentConditionInterface
{
    public function __construct(
        private readonly PaymentConditionInterface $operand1,
        private readonly PaymentConditionInterface $operand2
    ) {
    }

    public function isSatisfyBy(PaymentSystem $paymentSystem, PaymentMethodInterface $paymentMethod): bool
    {
        return $this->operand1->isSatisfyBy($paymentSystem, $paymentMethod) && $this->operand2->isSatisfyBy(
                $paymentSystem,
                $paymentMethod
            );
    }
}