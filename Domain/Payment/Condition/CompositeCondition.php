<?php

declare(strict_types=1);

namespace Domain\Payment\Condition;

use Domain\Payment\Aggregate\PaymentMethodInterface;
use Domain\Payment\Aggregate\PaymentSystem;

final class CompositeCondition implements PaymentConditionInterface
{
    private array $conditions;

    public function __construct(PaymentConditionInterface ...$conditions)
    {
        $this->conditions = $conditions;
    }

    public function isSatisfyBy(PaymentSystem $paymentSystem, PaymentMethodInterface $paymentMethod): bool
    {
        foreach ($this->conditions as $condition) {
            if ($condition->isSatisfyBy($paymentSystem, $paymentMethod)) {
                return false;
            }
        }

        return true;
    }
}