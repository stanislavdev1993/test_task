<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

class Settings
{
    public function __construct(
        private readonly bool $enabled = true,
        private readonly array $allowedCountries = [],
        private readonly array $excludedCountries = [],
        private readonly string $minAmount = '0.3',
        private readonly string $maxAmount = '3000',
        private readonly array $allowedLanguages = [],
        private readonly array $allowedOS = []
    )
    {
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function allowedCountries(): array
    {
        return $this->allowedCountries;
    }

    public function excludedCountries(): array
    {
        return $this->excludedCountries;
    }

    public function minAmount(): string
    {
        return $this->minAmount;
    }

    public function maxAmount(): string
    {
        return $this->maxAmount;
    }

    public function allowedLanguages(): array
    {
        return $this->allowedLanguages;
    }

    public function allowedOS(): array
    {
        return $this->allowedOS;
    }
}