<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate\PaymentMethodInfo;

interface PaymentMethodInfoInterface
{
    public function getTitle(): string;
    public function getUserTitle(): string;
    public function getPayUrl(): string;
}