<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

use Domain\Payment\Aggregate\PaymentMethodInfo\PaymentMethodInfoInterface;

final class PaymentMethodInfo implements PaymentMethodInfoInterface
{
    public function __construct(
        private readonly string $title,
        private readonly string $userTitle,
        private readonly string $payUrl
    )
    {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUserTitle(): string
    {
        return empty($this->userTitle) ? $this->title : $this->userTitle;
    }

    public function getPayUrl(): string
    {
        return $this->payUrl;
    }
}