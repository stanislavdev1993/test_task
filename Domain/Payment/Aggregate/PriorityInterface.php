<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

interface PriorityInterface
{
    public function getValue(): int;
}