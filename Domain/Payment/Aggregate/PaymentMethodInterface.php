<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

use Domain\Payment\Aggregate\PaymentMethodInfo\PaymentMethodInfoInterface;

interface PaymentMethodInterface
{
    public function getId(): string;
    public function getInfo(): PaymentMethodInfoInterface;
    public function getCommission(): Commission;
    public function getSettings(): Settings;
    public function getPriority(): PriorityInterface;
}