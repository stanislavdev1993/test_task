<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

class Commission
{
    public function __construct(private readonly string $value)
    {
    }

    public function getValue(): string
    {
        return $this->value;
    }
}