<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

final class Priority implements PriorityInterface
{
    public function __construct(private readonly int $value)
    {
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
