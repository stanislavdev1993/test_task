<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

use Domain\Payment\Aggregate\PaymentMethodInfo\PaymentMethodInfoInterface;

final class PaymentMethod implements PaymentMethodInterface
{
    public function __construct(
        private readonly string $id,
        private readonly PaymentMethodInfoInterface $info,
        private readonly Commission $commission,
        private readonly PriorityInterface $priority,
        private readonly Settings $settings
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getInfo(): PaymentMethodInfoInterface
    {
        return $this->info;
    }

    public function getCommission(): Commission
    {
        return $this->commission;
    }

    public function getPriority(): PriorityInterface
    {
        return $this->priority;
    }

    public function getSettings(): Settings
    {
        return $this->settings;
    }
}