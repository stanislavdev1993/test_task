<?php

declare(strict_types=1);

namespace Domain\Payment\Aggregate;

final class PaymentSystem
{
    public function __construct(
        private readonly string $id,
        private string $title,
        private string $description,
        private PaymentMethodCollection $paymentMethods,
        private Settings $settings
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPaymentMethods(): PaymentMethodCollection
    {
        return $this->paymentMethods;
    }

    public function getSettings(): Settings
    {
        return $this->settings;
    }
}