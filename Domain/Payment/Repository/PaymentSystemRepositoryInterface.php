<?php

declare(strict_types=1);

namespace Domain\Payment\Repository;

use Domain\Payment\Aggregate\PaymentSystemCollection;

interface PaymentSystemRepositoryInterface
{
    public function findAll(): PaymentSystemCollection;
}