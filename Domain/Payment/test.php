<?php

declare(strict_types=1);

$methods = new \Domain\Payment\Aggregate\PaymentMethodCollection([
    new \Domain\Payment\Aggregate\PaymentMethod(
        'sahidhsahdas',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'Bank card',
            'Privat bank card',
            'https://payment.url'
        ),
        new \Domain\Payment\Aggregate\Commission('2.5'),
        new \Domain\Payment\Aggregate\Priority(1),
        new \Domain\Payment\Aggregate\Settings(),
    ),
    new \Domain\Payment\Aggregate\PaymentMethod(
        'dsfdgdjgfg',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'LiqPay',
            '',
            'https://liqpay-payment.url'
        ),
        new \Domain\Payment\Aggregate\Commission('2'),
        new \Domain\Payment\Aggregate\Priority(2),
        new \Domain\Payment\Aggregate\Settings()
    ),
    new \Domain\Payment\Aggregate\PaymentMethod(
        'dsjfiosdf',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'Терминалы IBOX',
            '',
            'https://liqpay-payment.url'
        ),
        new \Domain\Payment\Aggregate\Commission('4'),
        new \Domain\Payment\Aggregate\Priority(3),
        new \Domain\Payment\Aggregate\Settings()
    )
]);

$interkassa = new \Domain\Payment\Aggregate\PaymentSystem(
    'sadishaodhsa',
    'Interkassa',
    'Interkassa payment gateway',
    $methods,
    new \Domain\Payment\Aggregate\Settings()
);

$methods = new \Domain\Payment\Aggregate\PaymentMethodCollection([
    new \Domain\Payment\Aggregate\PaymentMethod(
        'dsfwrewrwe',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'Локальные карты Индии',
            '',
            'https://local-indian-card.url'
        ),
        new \Domain\Payment\Aggregate\Commission('15'),
        new \Domain\Payment\Aggregate\Priority(3),
        new \Domain\Payment\Aggregate\Settings()
    ),
    new \Domain\Payment\Aggregate\PaymentMethod(
        'dsfhisduhf',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'Карты VISA / MasterCard',
            '',
            'https://visa-mastercard.ulr'
        ),
        new \Domain\Payment\Aggregate\Commission('3'),
        new \Domain\Payment\Aggregate\Priority(4),
        new \Domain\Payment\Aggregate\Settings()
    ),
    new \Domain\Payment\Aggregate\PaymentMethod(
        'dsfewer',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'Яндекс.Кошелек',
            '',
            'https://yandex.ulr'
        ),
        new \Domain\Payment\Aggregate\Commission('15'),
        new \Domain\Payment\Aggregate\Priority(5),
        new \Domain\Payment\Aggregate\Settings()
    ),
    new \Domain\Payment\Aggregate\PaymentMethod(
        'fdsfsdfsd',
        new \Domain\Payment\Aggregate\PaymentMethodInfo(
            'QIWI-кошелек',
            '',
            'https://qiwi.ulr'
        ),
        new \Domain\Payment\Aggregate\Commission('3.7'),
        new \Domain\Payment\Aggregate\Priority(5),
        new \Domain\Payment\Aggregate\Settings()
    ),
]);

$payU = new \Domain\Payment\Aggregate\PaymentSystem(
    'sadsdwqjj',
    'PayU',
    'Some description',
    $methods,
    new \Domain\Payment\Aggregate\Settings()
);