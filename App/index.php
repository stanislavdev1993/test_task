<?php

declare(strict_types=1);

// Входные параметры, которые откуда-то приходят, неважно откуда
$productType        = 'book';       // book | reward | walletRefill (пополнение внутреннего кошелька)
$amount             = 85.10;        // any float > 0
$lang               = 'en';         // only en | es | uk
$countryCode        = 'IN';         // any country code in ISO-3166 format
$userOs             = 'android';    // android | ios | windows

// Вам нужно сделать логику класса PaymentTypeSelector (можете назвать иначе, если хотите)
$paymentTypeSelector = new PaymentTypeSelector($productType, $amount, $lang, $countryCode, $userOs);
$paymentButtons = $paymentTypeSelector->getButtons();

/***
 * @var \Domain\Payment\Contact\PaymentServiceInterface $service
 */

$service = '';

$forever = $service->find();

// Где-то в шаблонах генерируется HTML для отображения кнопок.
// Там потребуются такие данные
foreach ($paymentButtons as $btn) {
    echo $btn->getName();
    echo $btn->getCommission();
    echo $btn->getImageUrl();
    echo $btn->getPayUrl();
}